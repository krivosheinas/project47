import uuid
from bs4 import BeautifulSoup
import requests


class Doctor:
    def __init__(self, title, description):
        self.guid = uuid.uuid4()
        self.title = title
        self.description = description

url = "https://www.profguide.io/professions/category/medicine/"

response = requests.get(url)

soup = BeautifulSoup(response.content, features="lxml")
trs = soup.find_all("tr")
with open('result.txt', 'w',  encoding="utf-8") as f:
    for tr in trs[1:]:
        tds = tr.find_all('td')
        title  = tds[0].a.text
        description = tds[2].text
        s = 'new Specialty { Id=new Guid("' + str(uuid.uuid4()) + '"), Title="' + title + '", Description="' + description + '"},\n'
        f.write(s)





