import uuid
from bs4 import BeautifulSoup
import requests
from russian_names import RussianNames
import json


person = RussianNames(gender=1.0).get_person()
items = person.split()
first_name = items[0]
mid_name = items[1]
last_name = items[2]

with open('doctors_result.txt', 'w',  encoding="utf-8") as f:
    # response_female = requests.get("https://randomuser.me/api/?gender=female&results=50")
    for i in range(50):
        person = RussianNames(gender=0.0).get_person()
        items = person.split()
        first_name = items[0]
        mid_name = items[1]
        last_name = items[2]
        # url = response_female.json()['results'][i]['picture']['large']
        url = f"https://randomuser.me/api/portraits/women/{1+2*i}.jpg"
        s = 'new Doctor{ Id = new Guid("'+ str(uuid.uuid4()) + '"), FirstName = "'+ first_name + '", MiddleName = "'+ mid_name +'", LastName = "' + last_name + '", Accepted=true, ImageUrl = "'+ url +'", StartOfWorkExperience = _start.AddDays(_random.Next(_range)), SpecialityId = Specialties.ElementAt(_random.Next(0,Specialties.Count())).Id  },\n'
        f.write(s)

    # response_male = requests.get("https://randomuser.me/api/?gender=male&results=50")
    for i in range(50):
        person = RussianNames(gender=1.0).get_person()
        items = person.split()
        first_name = items[0]
        mid_name = items[1]
        last_name = items[2]
        # url = response_male.json()['results'][i]['picture']['large']
        url = f"https://randomuser.me/api/portraits/men/{1+2*i}.jpg"
        s = 'new Doctor{ Id = new Guid("'+ str(uuid.uuid4()) + '"), FirstName = "'+ first_name + '", MiddleName = "'+ mid_name +'", LastName = "' + last_name + '", Accepted=true, ImageUrl = "'+ url +'", StartOfWorkExperience = _start.AddDays(_random.Next(_range)), SpecialityId = Specialties.ElementAt(_random.Next(0,Specialties.Count())).Id  },\n'
        f.write(s)





