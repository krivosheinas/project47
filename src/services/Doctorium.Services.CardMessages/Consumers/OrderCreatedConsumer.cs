﻿using Doctorium.Services.CardMessages.Core.Services;
using MassTransit;
using Messages;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.Consumers
{
    public class OrderCreatedConsumer : IConsumer<OrderCreated>
    {
        private readonly IOrderService _orderService;

        public OrderCreatedConsumer(IOrderService orderService)
        {
            _orderService = orderService;
        }

        public async Task Consume(ConsumeContext<OrderCreated> context)
        {
            await _orderService.CreateOrderAsync(context.Message.Id);
        }
    }
}
