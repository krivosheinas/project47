﻿using Doctorium.Services.CardMessages.Core.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }
        
        public DbSet<Message> Messages { get; set; }

        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Message>()
                .HasOne(x => x.Order)
                .WithMany(x => x.Messages)
                .HasForeignKey(x => x.OrderId);
        }
    }
}
