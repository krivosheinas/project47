﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.DataAccess.Data
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}
