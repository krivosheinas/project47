﻿using Doctorium.Services.CardMessages.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static List<Order> Orders => new List<Order>()
        {
            new Order()
            {
                Id = Guid.Parse("9710334a-57bd-4a4e-9e50-d23b80608778"),
                IsClosed = false
            }
        };
        
        public static List<Message> Messages => new List<Message>()
        {
            new Message()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                AuthorId = Guid.Parse("999a1424-7ee6-4d77-9724-7bec5e89b35e"),
                DateTime = (DateTime.Now).AddDays(-18),
                Text = "Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.",
                OrderId = Guid.Parse("9710334a-57bd-4a4e-9e50-d23b80608778")
            },
            new Message()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                AuthorId = Guid.Parse("ea588a8a-f70b-47bf-820e-0e5496b3dd9b"),
                DateTime = (DateTime.Now).AddDays(-17),
                Text = "Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн.",
                OrderId = Guid.Parse("9710334a-57bd-4a4e-9e50-d23b80608778")
            },
            new Message()
            {
                Id = Guid.Parse("0591254c-21e3-458c-99db-79783a7d9719"),
                AuthorId = Guid.Parse("ea588a8a-f70b-47bf-820e-0e5496b3dd9b"),
                DateTime = (DateTime.Now).AddDays(-16),
                Text = "Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.",
                OrderId = Guid.Parse("9710334a-57bd-4a4e-9e50-d23b80608778")
            },
            new Message()
            {
                Id = Guid.Parse("41a29fee-5638-41bd-ab78-33bb1addea43"),
                AuthorId = Guid.Parse("999a1424-7ee6-4d77-9724-7bec5e89b35e"),
                DateTime = (DateTime.Now).AddDays(-15),
                Text = "Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад.",
                OrderId = Guid.Parse("9710334a-57bd-4a4e-9e50-d23b80608778")
            },
            new Message()
            {
                Id = Guid.Parse("406f4fa1-11e7-46ad-b015-8a55bb0896b9"),
                AuthorId = Guid.Parse("ea588a8a-f70b-47bf-820e-0e5496b3dd9b"),
                DateTime = (DateTime.Now).AddDays(-14),
                Text = "Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney, штат Вирджиния, взял одно из самых странных слов в Lorem Ipsum, \"consectetur\", и занялся его поисками в классической латинской литературе.",
                OrderId = Guid.Parse("9710334a-57bd-4a4e-9e50-d23b80608778")
            },
            new Message()
            {
                Id = Guid.Parse("82ddf872-3da4-4ca2-a222-06e15fce570c"),
                AuthorId = Guid.Parse("999a1424-7ee6-4d77-9724-7bec5e89b35e"),
                DateTime = (DateTime.Now).AddDays(-13),
                Text = "В результате он нашёл неоспоримый первоисточник Lorem Ipsum в разделах 1.10.32 и 1.10.33 книги \"de Finibus Bonorum et Malorum\" (\"О пределах добра и зла\"), написанной Цицероном в 45 году н.э.",
                OrderId = Guid.Parse("9710334a-57bd-4a4e-9e50-d23b80608778")
            },
            new Message()
            {
                Id = Guid.Parse("63817b19-f82a-4fed-8e4e-472a273370e8"),
                AuthorId = Guid.Parse("999a1424-7ee6-4d77-9724-7bec5e89b35e"),
                DateTime = (DateTime.Now).AddDays(-12),
                Text = "Этот трактат по теории этики был очень популярен в эпоху Возрождения. Первая строка Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", происходит от одной из строк в разделе 1.10.32",
                OrderId = Guid.Parse("9710334a-57bd-4a4e-9e50-d23b80608778")
            },
        };
    }
}
