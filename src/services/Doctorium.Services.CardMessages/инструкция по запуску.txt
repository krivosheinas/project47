﻿Для запуска Базы Данных сервиса сообщений (Doctorium) + сам сервис нужно воспользоваться docker-compose файлом в каталоге с проектом Doctorium.Services.CardMessages

В сценарии файла заложен запуск БД postgres на базе образа postgres:13-alpine
Postgres поднимается на 5436 порту

После стартует сервис, он подключается к БД Doctorium и создает таблицы с данными Orders (заявки) и Messages (сообщения)
Сервис сообщений поднимается по порту 5005
http://localhost:5005/swagger/index.html

