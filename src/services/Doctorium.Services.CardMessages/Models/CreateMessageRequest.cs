﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.Models
{
    public class CreateMessageRequest
    {
        public Guid AuthorId { get; set; }

        public Guid OrderId { get; set; }

        public string Text { get; set; }
    }
}
