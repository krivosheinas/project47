﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.Models
{
    public class MessageResponse
    {
        public Guid AuthorId { get; set; }

        public Guid Id { get; set; }

        public DateTime DateTime { get; set; }

        public string Text { get; set; }
    }
}
