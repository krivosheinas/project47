﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.Models
{
    public class CreateOrderRequest
    {
        public Guid Id { get; set; }
    }
}
