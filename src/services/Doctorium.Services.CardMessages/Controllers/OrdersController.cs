﻿using Doctorium.Services.CardMessages.Core;
using Doctorium.Services.CardMessages.Core.Domain;
using Doctorium.Services.CardMessages.Core.Services;
using Doctorium.Services.CardMessages.Models;
using MassTransit;
using Messages;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrdersController : ControllerBase
    {
        private readonly IRepository<Order> _orderRepository;
        private readonly IOrderService _orderService;
        private readonly IPublishEndpoint _publishEndpoint;

        public OrdersController(IRepository<Order> orderRepository, IOrderService orderService, IPublishEndpoint publishEndpoint)
        {
            _orderRepository = orderRepository;
            _orderService = orderService;
            _publishEndpoint = publishEndpoint;
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync(CreateOrderRequest request)
        {
            await _orderService.CreateOrderAsync(request.Id);
            return Ok();
        }

        [HttpPost]
        [Route("{id}/close")]
        public async Task<IActionResult> CloseAsync(Guid id)
        {
            Order order = await _orderRepository.GetByIdAsync(id);

            if (order == null)
                return NotFound();

            order.IsClosed = true;

            await _orderRepository.UpdateAsync(order);
            await _publishEndpoint.Publish(new OrderClosed
            {
                Id = order.Id
            });

            return Ok();
        }

    }
}
