﻿using Doctorium.Services.CardMessages.Core;
using Doctorium.Services.CardMessages.Core.Domain;
using Doctorium.Services.CardMessages.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessagesController : ControllerBase
    {
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<Message> _messageRepository;

        public MessagesController(IRepository<Order> orderRepository, IRepository<Message> messageRepository)
        {
            _orderRepository = orderRepository;
            _messageRepository = messageRepository;
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync(CreateMessageRequest request)
        {
            Order order = await _orderRepository.GetByIdAsync(request.OrderId);

            if (order == null)
                return NotFound("Order not found");

            if (order.IsClosed)
                return BadRequest("Order is closed");

            var message = new Message
            {
                Id = Guid.NewGuid(),
                AuthorId = request.AuthorId,
                Order = order,
                OrderId = order.Id,
                DateTime = DateTime.Now,
                Text = request.Text
            };

            await _messageRepository.AddAsync(message);

            return Ok(new MessageResponse
                {
                    Id = message.Id,
                    AuthorId = message.AuthorId,
                    DateTime = message.DateTime,
                    Text = message.Text
                });
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateAsync(Guid id, UpdateMessageRequest request)
        {
            Message message = await _messageRepository.GetByIdAsync(id);

            if (message == null)
                return NotFound("Message not found");

            if (message.Order.IsClosed)
                return BadRequest("Order is closed");

            message.Text = request.Text;

            await _messageRepository.UpdateAsync(message);

            return Ok();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            Message message = await _messageRepository.GetByIdAsync(id);

            if (message == null)
                return NotFound("Message not found");

            if (message.Order.IsClosed)
                return BadRequest("Order is closed");

            await _messageRepository.DeleteAsync(message);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> QueryAsync([FromQuery] Guid orderId, [FromQuery] string from, [FromQuery] string to)
        {
            DateTime? dateFrom = from != null ? DateTime.Parse(from) : null;
            DateTime? dateTo = to != null ? DateTime.Parse(to) : null;

            List<Message> messages = (await _messageRepository
                .GetWhere(x => x.OrderId == orderId && (dateFrom.HasValue ? x.DateTime >= dateFrom : true) && (dateTo.HasValue ? x.DateTime < dateTo : true)))
                .OrderBy(x => x.DateTime).ToList();

            return Ok(messages
                .Select(x => new MessageResponse
                    {
                        Id = x.Id,
                        AuthorId = x.AuthorId,
                        DateTime = x.DateTime,
                        Text = x.Text
                    }));
        }
    }
}
