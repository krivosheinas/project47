﻿using System;

namespace Messages
{
    public class OrderClosed
    {
        public Guid Id { get; set; }
    }
}
