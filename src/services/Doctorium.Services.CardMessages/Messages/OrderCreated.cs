﻿using System;

namespace Messages
{
    public class OrderCreated
    {
        public Guid Id { get; set; }
    }
}
