﻿using Doctorium.Services.CardMessages.Core.Domain;
using System;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.Core.Services
{
    public class OrderService : IOrderService
    {
        private readonly IRepository<Order> _orderRepository;

        public OrderService(IRepository<Order> orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public async Task CreateOrderAsync(Guid id)
        {
            await _orderRepository.AddAsync(new Order { Id = id, IsClosed = false });
        }
    }
}
