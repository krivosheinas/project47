﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.Core.Services
{
    public interface IOrderService
    {
        Task CreateOrderAsync(Guid id);
    }
}
