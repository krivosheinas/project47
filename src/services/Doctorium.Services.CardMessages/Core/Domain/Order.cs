﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.Core.Domain
{
    public class Order : BaseEntity
    {
        public bool IsClosed { get; set; }

        public virtual ICollection<Message> Messages { get; set; }
    }
}
