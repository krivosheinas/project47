﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.Core.Domain
{
    public class Message : BaseEntity
    {
        public Guid AuthorId { get; set; }

        public Guid OrderId { get; set; }

        public virtual Order Order { get; set; }

        public DateTime DateTime { get; set; }

        public string Text { get; set; }
    }
}
