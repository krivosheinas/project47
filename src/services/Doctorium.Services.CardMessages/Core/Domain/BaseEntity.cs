﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
