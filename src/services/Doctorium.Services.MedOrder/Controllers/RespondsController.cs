﻿
using Doctorium.Services.MedOrder.DbContext.Entities;
using Doctorium.Services.MedOrder.DbContext.Interfaces;
using Doctorium.Services.MedOrder.DbContext.Repositories;
using Doctorium.Services.MedOrder.DbContext.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.MedRespond.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class RespondsController : ControllerBase
    {
        private readonly IBaseRepository<Respond> _respondRepo;
        private readonly RespondRepository _repo;

        public RespondsController(IBaseRepository<Respond> respondRepo, RespondRepository repo)
        {
            _respondRepo = respondRepo;
            _repo = repo;        
        }

        /// <summary>
        /// Получить список откликов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAllResponds()
        {
            var Responds = await _respondRepo.GetAllAsync();
            return Ok(Responds);
        }

        /// <summary>
        /// Получение отклика по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRespondById(Guid id)
        {
            var Respond = await _respondRepo.GetByIdAsync(id);
            if (Respond != null)
            {
                return Ok(Respond);
            }
            return NotFound();
        }

        /// <summary>
        /// Удаление отклика
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRespondById(Guid id)
        {
            var Respond = await _respondRepo.GetByIdAsync(id);
            if (Respond != null)
            {
                await _respondRepo.DeleteAsync(Respond);
            }
            return NotFound();
        }

        /// <summary>
        /// Создание нового отклика
        /// </summary>
        /// <param name="respond"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddNewRespond(Respond respond)
        {
            respond.DateCreated = DateTime.Now;
            await _respondRepo.AddAsync(respond);
            return Created(nameof(Respond), respond);
        }

        /// <summary>
        /// Редактирование отклика
        /// </summary>
        /// <param name="id"></param>
        /// <param name="Respond"></param>
        /// <returns></returns>
        [HttpPatch("{id}")]
        public async Task<IActionResult> EditRespond(Guid id, [FromBody] Respond Respond)
        {
            var existingRespond = await _respondRepo.GetByIdAsync(id);

            if (existingRespond != null)
            {
                await _respondRepo.Replace(existingRespond, Respond);
                return NoContent();
            }
            return NotFound();
        }

        /// <summary>
        /// Поиск откликов по заявке
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost("view")]
        public async Task<IActionResult> ViewOrder(RespondSearchFilter filter)
        {
            var orderId = filter.OrderId;

            var model = await _respondRepo.View(o => o.OrderId == filter.OrderId, filter.CurrentPage, filter.ShowCount, filter.Sort);

            return Ok(model);
        }

        [HttpPost("{id}/accept")]
        public async Task<IActionResult> AcceptRespond(Guid id)
        {
            var respond = await _respondRepo.GetByIdAsync(id);
            
            if(respond == null)
                return NotFound();

            try
            {
                await _repo.AcceptRespondAsync(respond);
                return Ok();
            }
            catch (Exception err)
            {
                return BadRequest(err.Message);
            }
            
        }
    }
}
