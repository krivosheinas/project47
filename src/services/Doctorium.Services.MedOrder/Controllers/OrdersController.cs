﻿using Doctorium.Services.MedOrder.DbContext.Entities;
using Doctorium.Services.MedOrder.DbContext.Interfaces;
using Doctorium.Services.MedOrder.DbContext.ViewModel;
using MassTransit;
using Messages;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.MedOrder.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IBaseRepository<Order> _orderRepo;
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly IBaseRepository<Respond> _respondRepo;

        public OrdersController(
            IBaseRepository<Order> orderRepo,
            IPublishEndpoint publishEndpoint,
            IBaseRepository<Respond> respondRepo)
        {
            _orderRepo = orderRepo;
            _publishEndpoint = publishEndpoint;
            _respondRepo = respondRepo;
        }

        /// <summary>
        /// Получить список заявок
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetOrders(Guid? patientId, bool? free, Guid? doctorId, bool? isClosed)
        {
            var orders = (await _orderRepo.GetWhere(x => (patientId.HasValue ? x.PatientId == patientId : true) 
                                                            && (free.HasValue ? ( free.Value ? x.DoctorId == null : x.DoctorId != null) : true)
                                                            && (doctorId.HasValue ? x.DoctorId == doctorId : true)
                                                            && (isClosed.HasValue ? x.IsClosed == isClosed : true)))
                .OrderBy(x => x.DateCreated);
            return Ok(orders);
        }

        /// <summary>
        /// Получение заявки по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrderById(Guid id)
        {
            var order = await _orderRepo.GetByIdAsync(id);
            if (order != null) {
                return Ok(order);
            }
            return NotFound();
        }

        /// <summary>
        /// Получение откликов по Id заявки
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/responds")]
        public async Task<IActionResult> GetResponds(Guid id)
        {
            var responds = (await _respondRepo.GetWhere(x => x.OrderId == id)).OrderBy(x => x.DateCreated);
            return Ok(responds);
        }

        /// <summary>
        /// Удаление заявки на лечение
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrderById(Guid id)
        {
            var order = await _orderRepo.GetByIdAsync(id);
            if (order != null)
            {
                await _orderRepo.DeleteAsync(order);
                return Ok();
            }
            return NotFound();
        }

        /// <summary>
        /// Создание новой заявки на лечение
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddNewOrder(Order order)
        {
            order.DateCreated = DateTime.Now;
            await _orderRepo.AddAsync(order);
            await _publishEndpoint.Publish(new OrderCreated
            {
                Id = order.Id
            });
            return Created(nameof(Order), order);
        }

        /// <summary>
        /// Редактирование заявки на лечение
        /// </summary>
        /// <param name="id"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        [HttpPatch("{id}")]
        public async Task<IActionResult> EditOrder(Guid id, [FromBody] Order order)
        {
            var existingOrder = await _orderRepo.GetByIdAsync(id);

            if (existingOrder != null)
            {
                existingOrder.Symptoms = order.Symptoms;
                existingOrder.City = order.City;
                await _orderRepo.UpdateAsync(existingOrder);
                return NoContent();
            }
            return NotFound();
        }

        [HttpPost("view")]
        public async Task<IActionResult> ViewOrder(OrderSearchFilter filter)             
        {
            var city = filter.City;

            var model = new ViewModel<Order>();

            if (city != null)
            {
                city = city.ToLower().Trim();
                model = await _orderRepo.View(o => o.City.ToLower() == city, filter.CurrentPage, filter.ShowCount, filter.Sort);
            }
            else
            {
                model = await _orderRepo.View(null, filter.CurrentPage, filter.ShowCount, filter.Sort);
            }           
            
                       
            return Ok(model);
        }
    }
}
