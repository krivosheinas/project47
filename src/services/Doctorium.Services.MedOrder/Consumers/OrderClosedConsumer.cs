﻿using Doctorium.Services.MedOrder.DbContext.Entities;
using Doctorium.Services.MedOrder.DbContext.Interfaces;
using MassTransit;
using Messages;
using System.Threading.Tasks;

namespace Doctorium.Services.CardMessages.Consumers
{
    public class OrderClosedConsumer : IConsumer<OrderClosed>
    {
        private readonly IBaseRepository<Order> _orderRepo;

        public OrderClosedConsumer(IBaseRepository<Order> orderRepo)
        {
            _orderRepo = orderRepo;
        }

        public async Task Consume(ConsumeContext<OrderClosed> context)
        {
            var order = await _orderRepo.GetByIdAsync(context.Message.Id);
            order.IsClosed = true;
            await _orderRepo.UpdateAsync(order);
        }
    }
}
