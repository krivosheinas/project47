﻿using System.Threading.Tasks;

namespace Doctorium.Services.MedOrder.DbContext
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly MedOrderDbContext _dataContext;

        public EfDbInitializer(MedOrderDbContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
            
            _dataContext.AddRange(LocalDataFactory.Orders);
            _dataContext.AddRange(LocalDataFactory.Responds);
            _dataContext.SaveChanges();                     
        }
    }
}