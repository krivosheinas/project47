﻿namespace Doctorium.Services.MedOrder.DbContext
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}