﻿using Microsoft.EntityFrameworkCore;
using Doctorium.Services.MedOrder.DbContext.Entities;

namespace Doctorium.Services.MedOrder.DbContext
{
    public class MedOrderDbContext: Microsoft.EntityFrameworkCore.DbContext
    {
        DbSet<Order> Orders { get; set; }

        DbSet<Respond> Responds { get; set; }

        public MedOrderDbContext(DbContextOptions<MedOrderDbContext> options)
            : base(options)
        {

        }
    }
}
