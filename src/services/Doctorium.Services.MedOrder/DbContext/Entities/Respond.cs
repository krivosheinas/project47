﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.MedOrder.DbContext.Entities
{   
    /// <summary>
    /// Отклик врача на заявку
    /// </summary>
    public class Respond: BaseEntity
    {

        /// <summary>
        /// Id заявки
        /// </summary>
        public Guid OrderId  { get; set; }

        /// <summary>
        /// Id врача 
        /// </summary>        
        public Guid DoctorId { get; set; }

        /// <summary>
        /// Текст отклика        
        /// </summary>
        [MaxLength(512)]
        [Required]
        public string Comment { get; set; } 
    }

}

