﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Doctorium.Services.MedOrder.DbContext.Entities
{
    /// <summary>
    /// Заявка на медицинское обслуживание
    /// </summary>
    public class Order: BaseEntity
    {
        /// <summary>
        /// Id пациента - инициатора заявки
        /// </summary>
        public Guid PatientId { get; set; }
        
        /// <summary>
        /// Id доктора
        /// </summary>
        public Guid? DoctorId { get; set; }
        
        /// <summary>
        /// Описание симптомов
        /// </summary>
        [MaxLength(512)]
        public string Symptoms { get; set; }
        
        /// <summary>
        /// Город, в котором может быть оказана услуга в приеме врача
        /// </summary>
        [MaxLength(100)]
        public string City { get; set; }
        
        /// <summary>
        /// Логический признак, нуждается ли пациент в очном приёме  
        /// </summary>
        public bool IsNeedsToSeeADoctor { get; set; }

        public bool IsClosed { get; set; }
    }
}