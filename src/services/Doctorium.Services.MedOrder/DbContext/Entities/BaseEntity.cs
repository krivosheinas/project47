﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Doctorium.Services.MedOrder.DbContext.Entities
{
    /// <summary>
    /// Базовый класс для всех объектов БД
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// Id объекта
        /// </summary>
        public Guid Id { get; set; }

        public DateTime DateCreated {get; set;}
    }
}