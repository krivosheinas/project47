﻿using Doctorium.Services.MedOrder.DbContext.Entities;
using Doctorium.Services.MedOrder.DbContext.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Doctorium.Services.MedOrder.DbContext.ViewModel;
using System.Linq.Expressions;

namespace Doctorium.Services.MedOrder.DbContext.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {

        private readonly MedOrderDbContext _db;

        public BaseRepository(MedOrderDbContext db)
        {
            _db = db;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<T> FindByConditionAsync(Func<T, bool> predicate)
        {
            return await _db.Set<T>().Where(x => predicate(x)).FirstOrDefaultAsync();
        }

        public async Task AddAsync(T entity)
        {
            await _db.Set<T>().AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            _db.Set<T>().Remove(entity);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = await GetByIdAsync(id);
            await DeleteAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _db.Set<T>().ToListAsync();
        }

        public async Task Replace(T oldEntity, T newEntity)
        {
            _db.Entry(oldEntity).CurrentValues.SetValues(newEntity);
            await _db.SaveChangesAsync();
        }

        public async Task<ViewModel<T>> View(Expression<Func<T,bool>> expression, int currentPage, int pageSize, bool sort)           
        {

            IQueryable<T> source = (expression == null) ? _db.Set<T>() : _db.Set<T>().Where(expression);                       
            var count = await source.CountAsync();
            var totalPages = (int)Math.Ceiling(count / (double)pageSize);
            var items =
                (sort) ?
                await source.Skip((currentPage - 1) * pageSize).Take(pageSize).OrderBy(x=>x.DateCreated).ToListAsync() :
                await source.Skip((currentPage - 1) * pageSize).Take(pageSize).OrderByDescending(x=>x.DateCreated).ToListAsync();
            return new ViewModel<T>()
            {
                CurrentPage = currentPage,
                TotalPages = totalPages,
                RecordsCount = count,
                Items = items
            };


        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await _db.Set<T>().Where(predicate).ToListAsync();
        }
    }
}
