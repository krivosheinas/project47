﻿using Doctorium.Services.MedOrder.DbContext.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.MedOrder.DbContext.Repositories
{
    public class RespondRepository
    {
        private MedOrderDbContext _dbContext;

        public RespondRepository(MedOrderDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Подтверждение отклика, значит заявке будет назначен доктор!
        /// </summary>
        /// <param name="respond"></param>
        /// <returns></returns>
        public async Task<Guid> AcceptRespondAsync (Respond respond)
        {
            var order = _dbContext.Set<Order>().FirstOrDefault(x => x.Id == respond.OrderId);

            if (order !=null)
            {
                order.DoctorId = respond.DoctorId;
                try
                {
                    await _dbContext.SaveChangesAsync();
                    return order.Id;
                }
                catch (Exception err)
                {
                    throw new Exception(err.Message);
                }
            }
            else
            {
                throw new Exception("Заявка не найдена");
            }
        }
        
    }
}
