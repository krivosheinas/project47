﻿using Doctorium.Services.MedOrder.DbContext.Entities;
using Doctorium.Services.MedOrder.DbContext.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Doctorium.Services.MedOrder.DbContext.Interfaces
{  

    public interface IBaseRepository<T>
        where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(Guid id);

        Task AddAsync(T entity);

        Task UpdateAsync(T entity);

        Task DeleteAsync(T entity);

        Task Replace(T oldEntity, T newEntity);

        Task<ViewModel<T>> View(Expression<Func<T, bool>> expression, int currentPage, int pageSize, bool sort);

        Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate);
    }
}
