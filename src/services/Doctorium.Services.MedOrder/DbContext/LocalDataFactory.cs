﻿using System;
using System.Collections.Generic;
using System.Linq;
using Doctorium.Services.MedOrder.DbContext.Entities;

namespace Doctorium.Services.MedOrder.DbContext
{
    public static class LocalDataFactory
    {
        //Заявки на лечение
        public static List<Order> Orders = new List<Order>()
        {
            new Order()
            {
                Id = Guid.Parse("9710334a-57bd-4a4e-9e50-d23b80608778"),
                PatientId = Guid.Parse("ea588a8a-f70b-47bf-820e-0e5496b3dd9b"),
                IsNeedsToSeeADoctor = true,
                City = "Москва",
                Symptoms = "На протяжении пяти дней наблюдается температура 38.5, кашель, боль в груди",
                DateCreated = (DateTime.Now).AddDays(-20)
            },
            new Order()
            {
                Id = Guid.Parse("a5acaf7f-357b-40b2-941a-146796db8a51"),
                PatientId = Guid.Parse("ea588a8a-f70b-47bf-820e-0e5496b3dd9b"),
                IsNeedsToSeeADoctor = false,
                City = "Москва",
                Symptoms = "Повышенная утомляемость, сонливость, головокружение, тошнота",
                DateCreated = (DateTime.Now).AddDays(-2)
            },
             new Order()
            {
                Id = Guid.Parse("4af08c8e-bc42-4902-bc93-8fee63f6f325"),
                PatientId = Guid.Parse("c5c2a961-2b5b-4f45-9871-646817fa17b9"),
                IsNeedsToSeeADoctor = false,
                City = "Тула",
                Symptoms = "Опухлость в области шеи, повышенное артериальное давление",
                DateCreated = (DateTime.Now).AddDays(-5)
            },

            new Order()
            {
                Id = Guid.Parse("04274a6c-10a6-4812-bc3c-c14d88af99a7"),
                PatientId = Guid.Parse("369ac3c3-a2c9-4efb-9213-c36ef4968eaa"),
                DoctorId = Guid.Parse("6b708101-3e5e-4c11-b172-a2fa748e18d3"),
                IsNeedsToSeeADoctor = true,
                City = "Новосибирск",
                Symptoms = "Повышенная утомляемость, сонливость, головокружение, тошнота",
                 DateCreated = (DateTime.Now).AddDays(-2)
            },
            new Order()
            {
                Id = Guid.Parse("862848c0-8be0-4012-bf0e-37d2d26f9ea0"),
                PatientId = Guid.Parse("01bbd237-6b67-4e40-8121-e5ee9a97c3ea"),
                DoctorId = Guid.Parse("88c1352d-39f8-4272-b15a-6f06c8368ff8"),
                IsNeedsToSeeADoctor = false,
                City = "Саратов",
                Symptoms = "Бессоница, отсутствие обоняния, слабость, круги под глазами",
                 DateCreated = (DateTime.Now).AddDays(-6)
            },
            new Order()
            {
                Id = Guid.Parse("9f576483-d6fb-4ddd-a42b-b844c9d4a218"),
                PatientId = Guid.Parse("dd93dbe4-6de7-47c9-9eb7-3f9889847a18"),
                IsNeedsToSeeADoctor = true,
                City = "Сургут",
                Symptoms = "Носовое кровотечение, головная боль",
                DateCreated = DateTime.Now
            }
        };

        //Отклики
        public static List<Respond> Responds = new List<Respond>()
        {
            new Respond()
            {
                Id = Guid.Parse("95709ccb-5479-4d98-82ed-4b09ba0540ff"),
                OrderId = Guid.Parse("9710334a-57bd-4a4e-9e50-d23b80608778"),
                DoctorId = Guid.Parse("999a1424-7ee6-4d77-9724-7bec5e89b35e"),
                Comment = "Необходимо срочно показаться к специалисту, готов проконсультировать, стоимость услуги 1000 руб.",
                DateCreated = (DateTime.Now).AddDays(-19)
            },
            new Respond()
            {
                Id = Guid.Parse("738e6e30-6e97-43e9-ab3c-e5a51aa3a068"),
                OrderId = Guid.Parse("9710334a-57bd-4a4e-9e50-d23b80608778"),
                DoctorId = Guid.Parse("71f5a979-0585-4841-b0f8-b9ce9f35068b"),
                Comment = "Принимайте аспирин, будете здоровы!",
                DateCreated = (DateTime.Now).AddDays(-3)
            },
            new Respond()
            {
                Id = Guid.Parse("ee9dc2bf-f00c-420f-a3f0-5272a1384532"),
                OrderId = Guid.Parse("4af08c8e-bc42-4902-bc93-8fee63f6f325"),
                DoctorId = Guid.Parse("88c1352d-39f8-4272-b15a-6f06c8368ff8"),
                Comment = "Не стоит заниматься самолечением, готов Вас принять завтра в 17-00 в медицинском центре им. Парамонова",
                DateCreated = DateTime.Now
            },
        };
    }
}