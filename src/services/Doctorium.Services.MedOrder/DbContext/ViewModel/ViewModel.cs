﻿using System.Collections.Generic;

namespace Doctorium.Services.MedOrder.DbContext.ViewModel
{
    /// <summary>
    /// Модель представления
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ViewModel<T>
    {   /// <summary>
        /// Количество доступных страниц 
        /// </summary>
        public int TotalPages { get; set; }
        /// <summary>
        /// Текущая страница
        /// </summary>        
        public int CurrentPage { get; set; }
        /// <summary>
        /// Всего количество записей
        /// </summary>
        public int RecordsCount { get; set; }  
        /// <summary>
        /// Выборка
        /// </summary>
        public ICollection<T> Items { get; set; }
    }
    
}