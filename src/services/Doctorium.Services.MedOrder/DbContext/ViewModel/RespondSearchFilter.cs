﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.MedOrder.DbContext.ViewModel
{
    public class RespondSearchFilter: BaseSearchFilter
    {
        public Guid OrderId { get; set; }
    }
}
