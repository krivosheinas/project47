﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.MedOrder.DbContext.ViewModel
{
    public class BaseSearchFilter
    {
        public int CurrentPage { get; set; }        
        public int ShowCount { get; set; }

        public bool Sort { get; set; }
    }
}
