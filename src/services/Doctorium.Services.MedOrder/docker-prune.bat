docker-compose down
docker container prune -f
docker image rm doctoriumservicesmedorder_doctorium-medorders-api
docker volume rm doctoriumservicesmedorder_postgres_data
pause