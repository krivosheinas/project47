using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Services.Gateway
{
    public class Program
    {
        private static int _port = 5025; 
        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("host.json", true)
             .Build();

            var port = config.GetSection("HostSettings:port").Value;
            if (port !=null)
            {
                int.TryParse(port.ToString(), out _port);
            };
            
            
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(configuration=> configuration.AddJsonFile("ocelot.json", true,true))
                .ConfigureWebHostDefaults(webBuilder =>
                {  
                    webBuilder
                    .UseUrls($"https://localhost:{_port}")
                    .UseStartup<Startup>();
                });
    }
}
