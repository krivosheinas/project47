﻿using AutoMapper;
using Doctorium.Services.MedCard.DbContext.Entities;
using Doctorium.Services.MedCard.Model;

namespace Doctorium.Services.MedCard.MapperProfiles
{
    public class PatientControllerProfile : Profile
    {
        public PatientControllerProfile()
        {
            CreateMap<Patient, PatientModel>();

            CreateMap<CreatePatientModel, Patient>();
        }
    }
}