﻿using AutoMapper;
using Doctorium.Services.MedCard.DbContext.Entities;
using Doctorium.Services.MedCard.Model;

namespace Doctorium.Services.MedCard.MapperProfiles
{
    public class RecordMapperProfile : Profile
    {
        public RecordMapperProfile()
        {
            CreateMap<AddRecordModel, Record>();

            CreateMap<Record, RecordModel>();
        }
    }
}