﻿using System;

namespace Doctorium.Services.MedCard.Model
{
    public class RecordModel
    {
        public Guid Id { get; set; }
                
        public Guid DoctorId { get; set; }
        
        public string Text { get; set; }
        
        public DateTime Created { get; set; }
}
}