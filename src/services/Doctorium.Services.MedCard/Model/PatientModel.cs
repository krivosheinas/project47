﻿using System;
using System.Collections.Generic;

namespace Doctorium.Services.MedCard.Model
{
    public class PatientModel
    {
        public Guid Id { get; set; }
        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Patronymic { get; set; }

        public DateTime BirthDate { get; set; }

        public IList<RecordModel> Records { get; set; }
    }
}