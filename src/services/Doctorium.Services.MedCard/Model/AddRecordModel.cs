﻿using System;

namespace Doctorium.Services.MedCard.Model
{
    public class AddRecordModel
    {
        public Guid DoctorId { get; set; }
        
        public string Text { get; set; }
    }
}