﻿using System;

namespace Doctorium.Services.MedCard.Model
{
    public class CreatePatientModel
    {
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public string Patronymic { get; set; }

        public DateTime BirthDate { get; set; }
    }
}