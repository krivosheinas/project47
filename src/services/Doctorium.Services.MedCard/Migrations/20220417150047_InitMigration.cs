﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Doctorium.Services.MedCard.Migrations
{
    public partial class InitMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Patients",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    Patronymic = table.Column<string>(type: "text", nullable: true),
                    BirthDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Patients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Records",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    PatientId = table.Column<Guid>(type: "uuid", nullable: false),
                    DoctorId = table.Column<Guid>(type: "uuid", nullable: false),
                    Text = table.Column<string>(type: "text", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Records", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Records_Patients_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Patients",
                columns: new[] { "Id", "BirthDate", "FirstName", "LastName", "Patronymic" },
                values: new object[] { new Guid("ea588a8a-f70b-47bf-820e-0e5496b3dd9b"), new DateTime(1997, 1, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Сева", "Мозговой", "Святославович" });

            migrationBuilder.InsertData(
                table: "Records",
                columns: new[] { "Id", "Created", "DoctorId", "PatientId", "Text" },
                values: new object[] { new Guid("1b301132-5347-4c73-8f76-a8fd0dded403"), new DateTime(2022, 4, 17, 18, 0, 47, 499, DateTimeKind.Local).AddTicks(7523), new Guid("6b708101-3e5e-4c11-b172-a2fa748e18d3"), new Guid("ea588a8a-f70b-47bf-820e-0e5496b3dd9b"), "Пациент здоров" });

            migrationBuilder.CreateIndex(
                name: "IX_Records_PatientId",
                table: "Records",
                column: "PatientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Records");

            migrationBuilder.DropTable(
                name: "Patients");
        }
    }
}
