﻿using System;
using System.Collections.Generic;

namespace Doctorium.Services.MedCard.DbContext.Entities
{
    public class Patient : BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Patronymic { get; set; }

        public DateTime BirthDate { get; set; }

        public IList<Record> Records { get; set; }
    }
}