﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Doctorium.Services.MedCard.DbContext.Entities
{
    /// <summary>
    /// Базовый класс для всех объектов БД
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// Id объекта
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
    }
}