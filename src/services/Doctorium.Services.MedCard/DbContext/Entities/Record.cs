﻿using System;

namespace Doctorium.Services.MedCard.DbContext.Entities
{
    public class Record : BaseEntity
    {
        public Guid PatientId { get; set; }
        
        public Guid DoctorId { get; set; }
        
        public string Text { get; set; }
        
        public DateTime Created { get; set; }
    }
}