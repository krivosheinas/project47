﻿using System;
using System.Diagnostics;
using Doctorium.Services.MedCard.DbContext.Entities;
using Microsoft.EntityFrameworkCore;

namespace Doctorium.Services.MedCard.DbContext
{
    public class MedCardDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbSet<Patient> Patients { get; set; }
        
        public DbSet<Record> Records { get; set; }

        public MedCardDbContext(DbContextOptions<MedCardDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #if DEBUG
            modelBuilder.Entity<Patient>().HasData(
                new Patient
                {
                    FirstName = "Сева",
                    LastName = "Мозговой",
                    BirthDate = new DateTime(1997, 01, 02),
                    Id = Guid.Parse("ea588a8a-f70b-47bf-820e-0e5496b3dd9b"),
                    Patronymic = "Святославович"
                }
            );

            modelBuilder.Entity<Record>().HasData(
                new Record
                {
                    Id = Guid.NewGuid(),
                    Created = DateTime.Now,
                    DoctorId = Guid.Parse("6b708101-3e5e-4c11-b172-a2fa748e18d3"),
                    PatientId = Guid.Parse("ea588a8a-f70b-47bf-820e-0e5496b3dd9b"),
                    Text = "Пациент здоров"
                });
            #endif
        }
    }
}