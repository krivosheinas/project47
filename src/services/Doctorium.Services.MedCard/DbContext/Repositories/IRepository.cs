﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Doctorium.Services.MedCard.DbContext.Entities;

namespace Doctorium.Services.MedCard.DbContext.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<T> FindAsync(Guid id);

        Task<T> FindByConditionAsync(Func<T, bool> predicate);

        Task AddAsync(T entity);

        Task DeleteAsync(T entity);

        Task DeleteAsync(Guid id);

        Task UpdateAsync(T entity);

        Task LoadProperty<TProperty>(T entity, Expression<Func<T, IEnumerable<TProperty>>> property);
        
        Task LoadCollection<TProperty>(T entity, Expression<Func<T, IEnumerable<TProperty>>> property) where TProperty : class;

        Task<bool> AnyAsync(Guid id);
    }
}