﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Doctorium.Services.MedCard.DbContext.Entities;
using Microsoft.EntityFrameworkCore;

namespace Doctorium.Services.MedCard.DbContext.Repositories.Impl
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly MedCardDbContext _db;

        public Repository(MedCardDbContext db)
        {
            _db = db;
        }

        public async Task<T> FindAsync(Guid id)
        {
            return await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<T> FindByConditionAsync(Func<T, bool> predicate)
        {
            return await _db.Set<T>().Where(x => predicate(x)).FirstOrDefaultAsync();
        }

        public async Task AddAsync(T entity)
        {
            await _db.Set<T>().AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            _db.Set<T>().Remove(entity);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = await FindAsync(id);
            await DeleteAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            await _db.SaveChangesAsync();
        }

        public async Task LoadProperty<TProperty>(T entity, Expression<Func<T, IEnumerable<TProperty>>> property)
        {
            await _db.Entry(entity).Reference(p => property).LoadAsync();
        }

        public async Task LoadCollection<TProperty>(T entity, Expression<Func<T, IEnumerable<TProperty>>> property)
            where TProperty : class
        {
            await _db.Entry(entity).Collection(property).LoadAsync();
        }

        public async Task<bool> AnyAsync(Guid id)
        {
            return await _db.Set<T>().AnyAsync(r => r.Id == id);
        }
    }
}