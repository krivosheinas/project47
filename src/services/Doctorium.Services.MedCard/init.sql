﻿CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    "MigrationId" character varying(150) NOT NULL,
    "ProductVersion" character varying(32) NOT NULL,
    CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY ("MigrationId")
);

CREATE TABLE "Patients" (
    "Id" uuid NOT NULL,
    "FirstName" text NULL,
    "LastName" text NULL,
    "Patronymic" text NULL,
    "BirthDate" timestamp without time zone NOT NULL,
    CONSTRAINT "PK_Patients" PRIMARY KEY ("Id")
);

CREATE TABLE "Records" (
    "Id" uuid NOT NULL,
    "PatientId" uuid NOT NULL,
    "DoctorId" uuid NOT NULL,
    "Text" text NULL,
    "Created" timestamp without time zone NOT NULL,
    CONSTRAINT "PK_Records" PRIMARY KEY ("Id"),
    CONSTRAINT "FK_Records_Patients_PatientId" FOREIGN KEY ("PatientId") REFERENCES "Patients" ("Id") ON DELETE CASCADE
);

INSERT INTO "Patients" ("Id", "BirthDate", "FirstName", "LastName", "Patronymic")
VALUES ('ea588a8a-f70b-47bf-820e-0e5496b3dd9b', TIMESTAMP '1997-01-02 00:00:00', 'Сева', 'Мозговой', 'Святославович');

INSERT INTO "Records" ("Id", "Created", "DoctorId", "PatientId", "Text")
VALUES ('1b301132-5347-4c73-8f76-a8fd0dded403', TIMESTAMP '2022-04-17 18:00:47.499752', '6b708101-3e5e-4c11-b172-a2fa748e18d3', 'ea588a8a-f70b-47bf-820e-0e5496b3dd9b', 'Пациент здоров');

CREATE INDEX "IX_Records_PatientId" ON "Records" ("PatientId");

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20220417150047_InitMigration', '5.0.13');

