﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Doctorium.Services.MedCard.DbContext.Entities;
using Doctorium.Services.MedCard.DbContext.Repositories;
using Doctorium.Services.MedCard.MapperProfiles;
using Doctorium.Services.MedCard.Model;
using Microsoft.AspNetCore.Mvc;

namespace Doctorium.Services.MedCard.Controllers
{
    public class RecordController : Controller
    {
        private readonly IRepository<Record> _recordRepository;
        private readonly IRepository<Patient> _patientRepository;
        private readonly IMapper _mapper;

        public RecordController(IRepository<Record> recordRepository, IRepository<Patient> patientRepository)
        {
            _recordRepository = recordRepository;
            _patientRepository = patientRepository;
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<RecordMapperProfile>()));
        }
        
        [HttpPost("{patientId:guid}")]
        public async Task<IActionResult> AddRecord(Guid patientId, AddRecordModel addRecordModel)
        {
            var patient = await _patientRepository.AnyAsync(patientId);
            if (!patient)
            {
                return NotFound("Пациент не был найден");
            }
            var record = _mapper.Map<Record>(addRecordModel);
            record.PatientId = patientId;
            record.Created = DateTime.Now;
            await _recordRepository.AddAsync(record);
            return Ok();
        }
    }
}