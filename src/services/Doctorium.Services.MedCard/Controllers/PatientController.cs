﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Doctorium.Services.MedCard.DbContext.Entities;
using Doctorium.Services.MedCard.DbContext.Repositories;
using Doctorium.Services.MedCard.MapperProfiles;
using Doctorium.Services.MedCard.Model;
using Microsoft.AspNetCore.Mvc;

namespace Doctorium.Services.MedCard.Controllers
{
    [Route("[controller]")]
    public class PatientController : Controller
    {
        private readonly IRepository<Patient> _patientRepository;
        private readonly IMapper _mapper;

        public PatientController(IRepository<Patient> patientRepository)
        {
            _patientRepository = patientRepository;
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<PatientControllerProfile>();
                cfg.AddProfile<RecordMapperProfile>();
            }));
        }
        
        [HttpGet("test")]
        public async Task<IActionResult> Test()
        {
            return Json(new {Result = "Hello"});
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetPatient(Guid id)
        {
            var patient = await _patientRepository.FindAsync(id);
            if (patient == null)
            {
                return NotFound("Пациент не был найден");
            }
            await _patientRepository.LoadCollection(patient, p => p.Records);
            var result = _mapper.Map<PatientModel>(patient);
            return Json(result);
        }
        
        [HttpPut]
        public async Task<IActionResult> CreatePatient(CreatePatientModel model)
        {
            var patient = _mapper.Map<Patient>(model);
            patient.Id = Guid.NewGuid();
            await _patientRepository.AddAsync(patient);
            return Ok();
        }
    }
}