﻿using Microsoft.AspNetCore.Mvc;

namespace Doctorium.Services.Doctors.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController
    {

        public string[] Get()
        {
            return new[] { "Hello", "From", "Ivan" };
        }
    }
}
