﻿using System;
using System.Threading.Tasks;
using Doctorium.Services.Doctors.Data.Entities;

namespace Doctorium.Services.Doctors.Data
{
    public interface IDoctorRepository
    {
        // General
        void Add<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveChangesAsync();

        // Doctors
        Task<Doctor[]> GetAllDoctorsAsync(bool includeEducation = false);
        Task<Doctor> GetDoctorAsync(Guid id, bool includeEducation = false);

        //TODO: other entities

    }
}
