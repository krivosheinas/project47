﻿using System;

namespace Doctorium.Services.Doctors.Data.Entities
{
    public class Specialization
    {
        public Guid Id{ get; set; }
        public string Title { get; set; }
    }
}