﻿using System;
using System.Collections.Generic;

namespace Doctorium.Services.Doctors.Data.Entities
{
    public class Doctor
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string ImageSource { get; set; }
        public ICollection<Certificate> Certificates{ get; set; }
        public ICollection<EducationItem> EducationItems { get; set; }
        public ICollection<Specialization>Specializations { get; set; }
        public DateTime? StartOfWorkExperience { get; set; }
        public bool Accepted { get; set; }

    }
}
