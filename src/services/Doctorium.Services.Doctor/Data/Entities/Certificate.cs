﻿using System;

namespace Doctorium.Services.Doctors.Data.Entities
{
    public class Certificate
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public DateTime DateOfReceipt { get; set; }
    }
}