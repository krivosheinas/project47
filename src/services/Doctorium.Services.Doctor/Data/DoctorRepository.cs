﻿using Doctorium.Services.Doctors.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Doctorium.Services.Doctors.Data
{
    public class DoctorRepository : IDoctorRepository
    {
        private readonly DoctorContext _context;

        public DoctorRepository(DoctorContext context)
        {
            _context = context;
        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<Doctor[]> GetAllDoctorsAsync(bool includeEducation = false)
        {
            IQueryable<Doctor> query = _context.Doctors;

            if (includeEducation)
            {
                query = query
                    .Include(d => d.EducationItems);
            }

            query = query.OrderByDescending(d => d.LastName);

            return await query.ToArrayAsync();
        }

        public  async Task<Doctor> GetDoctorAsync(Guid id, bool includeEducation = false)
        {
            IQueryable<Doctor> query = _context.Doctors;

            if (includeEducation)
            {
                query = query
                    .Include(d => d.EducationItems);
            }

            query = query.Where(d => d.Id == id);

            return await query.FirstOrDefaultAsync();
        }

        public async Task<bool> SaveChangesAsync()
        {
            // Only return success if at least one row was changed
            return (await _context.SaveChangesAsync()) > 0;
        }
    }
}
