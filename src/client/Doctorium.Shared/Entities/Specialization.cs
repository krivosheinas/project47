﻿using System;

namespace Doctorium.Shared.Entities
{
    public class Specialization
    {
        public Guid Id{ get; set; }
        public string Title { get; set; }
    }
}