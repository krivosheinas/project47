﻿using System;

namespace Doctorium.Shared.Entities
{
    public class EducationItem
    {
        public Guid Id{ get; set; }
        public string Title { get; set; }
        public int DurationInMonthes { get; set; }
        public DateTime FinishDate { get; set; }
    }
}