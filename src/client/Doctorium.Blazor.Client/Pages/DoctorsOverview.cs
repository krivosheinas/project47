﻿using Doctorium.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Doctorium.Blazor.Client.Pages
{
    public partial class DoctorsOverview
    {
        public IEnumerable<Doctor> Doctors { get; set; }
        public IEnumerable<Certificate> Certificates { get; set; }
        public IEnumerable<EducationItem> EducationItems { get; set; }
        public IEnumerable<Specialization> Specializations { get; set; }

        protected override Task OnInitializedAsync()
        {
            InitializeCertificates();
            InitializeEducationItems();
            InitializeSpecializations();
            InitializeDoctors();

            return base.OnInitializedAsync();
        }

        private void InitializeSpecializations()
        {
            Specializations = new List<Specialization>()
            {
                new Specialization{ Id = new Guid("12FA647C-AD54-4BCC-A860-E5A2664B019D"), Title = "Кардиолог"},
                new Specialization{ Id = new Guid("22FA647C-AD54-4BCC-A860-E5A2664B019D"), Title = "Психиатр"},
                new Specialization{ Id = new Guid("32FA647C-AD54-4BCC-A860-E5A2664B019D"), Title = "Терапевт"},
                new Specialization{ Id = new Guid("42FA647C-AD54-4BCC-A860-E5A2664B019D"), Title = "Дерматолог"}
            };
        }

        private void InitializeCertificates()
        {
            Certificates = new List<Certificate>()
            {
                new Certificate{ Id = new Guid("61FA647C-AD54-4BCC-A860-E5A2664B019D"), Title = "Сертификат 1", DateOfReceipt = DateTime.Parse("03.03.2020") },
                new Certificate{ Id = new Guid("62FA647C-AD54-4BCC-A860-E5A2664B019D"), Title = "Сертификат 2", DateOfReceipt = DateTime.Parse("03.03.2020") },
                new Certificate{ Id = new Guid("63FA647C-AD54-4BCC-A860-E5A2664B019D"), Title = "Сертификат 3", DateOfReceipt = DateTime.Parse("03.03.2020") }
            };
        }

        private void InitializeDoctors()
        {
            Doctors = new List<Doctor>()
            {
                new Doctor{
                    Id = new Guid("62FA647C-AD54-4BCC-A860-E5A2664B011D"), FirstName = "Иван",
                    MiddleName = "Иванович", LastName = "Иванов", Accepted = true,
                    Certificates = new List<Certificate>() { (Certificates.FirstOrDefault(s => s.Title == "Сертификат 1")) },
                    ImageSource = "",
                    StartOfWorkExperience = DateTime.Now,
                    EducationItems = new List<EducationItem>(),
                    Specializations = new List<Specialization>(){Specializations.First() }
                },
                    new Doctor{
                    Id = new Guid("62FA647C-AD54-4BCC-A860-E5A2664B012D"), FirstName = "Ольга",
                    MiddleName = "Андреевна", LastName = "Петрова", Accepted = true,
                    Certificates = new List<Certificate>() { (Certificates.FirstOrDefault(s => s.Title == "Сертификат 1")) },
                    ImageSource = "",
                    StartOfWorkExperience = DateTime.Now,
                    EducationItems = new List<EducationItem>(),
                    Specializations = new List<Specialization>(){Specializations.First() }
                },
                    new Doctor{
                    Id = new Guid("62FA647C-AD54-4BCC-A860-E5A2664B013D"), FirstName = "Олег",
                    MiddleName = "Владимирович", LastName = "Сахаров", Accepted = true,
                    Certificates = new List<Certificate>() { (Certificates.FirstOrDefault(s => s.Title == "Сертификат 1")) },
                    ImageSource = "",
                    StartOfWorkExperience = DateTime.Now,
                    EducationItems = new List<EducationItem>(),
                    Specializations = new List<Specialization>(){Specializations.First() }
                },

            };
        }

        private void InitializeEducationItems()
        {
            EducationItems = new List<EducationItem>();
        }

    }
}
