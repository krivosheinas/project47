export interface OrderDto {
    id: string;
    dateCreated: Date;
    symptoms: string;
    city: string;
    isNeedsToSeeADoctor: boolean;
    doctorId: string | null;
    patientId: string;
}