export interface AuthDto {
    userType: "patient" | "doctor";
    userId: string;
}