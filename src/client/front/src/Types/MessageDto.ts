export interface MessageDto {
    authorId: string;
    id: string;
    dateTime: Date;
    text: string;
}