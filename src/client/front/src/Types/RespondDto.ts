export interface RespondDto {
    id: string;
    dateCreated: Date;
    doctorId: string;
    comment: string;
}