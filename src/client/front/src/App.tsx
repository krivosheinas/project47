import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { Home } from './Components/Home';
import Login from './Components/Login';
import { Patient } from './Components/Patient';
import { Doctor } from './Components/Doctor';
import { NotFound } from './Components/NotFound';
import Register from './Components/Register';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/Login">
          <Login />
        </Route>
        <Route path="/Register">
          <Register />
        </Route>
        <Route path="/Patient">
          <Patient />
        </Route>
        <Route path="/Doctor">
          <Doctor />
        </Route>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
