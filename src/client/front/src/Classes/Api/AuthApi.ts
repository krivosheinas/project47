import { AuthDto } from "../../Types/AuthDto";

export class AuthApi {
    public static async signIn(login: string, password: string): Promise<AuthDto> {
        //TODO Заменить это реальным вызовом бекэнда
        if (login === "patient")
            return Promise.resolve({ userType: 'patient', userId: 'ea588a8a-f70b-47bf-820e-0e5496b3dd9b'});
        
        if (login === "doctor")
            return Promise.resolve({ userType: 'doctor', userId: '999a1424-7ee6-4d77-9724-7bec5e89b35e'});    
        
        return Promise.reject();
    }

    public static async logOut(): Promise<void> {
        //TODO Заменить это реальным вызовом бекэнда
        return Promise.resolve();
    }
}