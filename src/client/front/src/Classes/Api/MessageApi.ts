import axios from "axios";
import { MessageDto } from "../../Types/MessageDto";
import { Auth } from "../Auth";

export class MessagesApi {
    public static async get(orderId: string): Promise<MessageDto[]> {
        return (await axios.get(`http://localhost:10000/messages?orderId=${orderId}`)).data;
    }

    public static async send(orderId: string, text: string): Promise<MessageDto> {
        return (await axios.post('http://localhost:10000/messages', { orderId, text, authorId: Auth.getUserId() })).data;
    }

    public static async delete(messageId: string): Promise<void> {
        await axios.delete(`http://localhost:10000/messages/${messageId}`);
    }

    public static async update(messageId: string, text: string): Promise<void> {
        await axios.put(`http://localhost:10000/messages/${messageId}`, { text });
    }
}