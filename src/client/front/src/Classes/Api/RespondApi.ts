import axios from "axios";
import { RespondDto } from "../../Types/RespondDto";
import { Auth } from "../Auth";

export class RespondApi {
    public static async getRespondsByOrderId(orderId: string): Promise<RespondDto[]> {
        return (await axios.get(`http://localhost:10001/orders/${orderId}/responds`)).data;
    }

    public static async accept(respondId: string): Promise<void> {
        await axios.post(`http://localhost:10001/responds/${respondId}/accept`);
    }

    public static async add(orderId: string, text: string): Promise<void> {
        await axios.post('http://localhost:10001/responds', {
            orderId,
            doctorId: Auth.getUserId(),
            comment: text
        });
    }
}