import { OrderDto } from "../../Types/OrderDto";
import axios from "axios";
import { Auth } from "../Auth";

export class OrderApi {
    public static async getPatientOrders(): Promise<OrderDto[]> {
        return (await axios.get(`http://localhost:10001/orders?patientId=${Auth.getUserId()}`)).data;
    }

    public static async getDoctorOrders(): Promise<OrderDto[]> {
        return (await axios.get(`http://localhost:10001/orders?doctorId=${Auth.getUserId()}&isClosed=false`)).data;
    }

    public static async getFreeOrders(): Promise<OrderDto[]> {
        return (await axios.get(`http://localhost:10001/orders?free=true`)).data;
    }

    public static async create(symptoms: string, city: string): Promise<void> {
        await axios.post('http://localhost:10001/orders', { symptoms, city, patientId: Auth.getUserId() });
    }

    public static async delete(orderId: string): Promise<void> {
        await axios.delete(`http://localhost:10001/orders/${orderId}`);
    }

    public static async edit(orderId: string, symptoms: string, city: string): Promise<void> {
        await axios.patch(`http://localhost:10001/orders/${orderId}`, { symptoms, city });
    }

    public static async getById(orderId: string): Promise<OrderDto> {
        return (await axios.get(`http://localhost:10001/orders/${orderId}`)).data;
    }

    public static async close(orderId: string): Promise<void> {
        await axios.post(`http://localhost:10000/orders/${orderId}/close`);
    }
}