import { AuthApi } from "./Api/AuthApi";

interface AuthData {
    userType: "patient" | "doctor" | null;
    userId: string | null;
}

export class Auth {
    public static isPatient(): boolean {
        return this.getAuthData().userType === "patient";
    }

    public static isDoctor(): boolean {
        return this.getAuthData().userType === "doctor";
    }

    private static getAuthData(): AuthData {
        const storageData = localStorage.getItem("Auth");
        if (storageData === null)
            return { userType: null, userId: null};
        return JSON.parse(storageData);
    }

    public static getUserId(): string {
        return this.getAuthData().userId!;
    }

    public static async signIn(login: string, password: string): Promise<void> {
        const authData = await AuthApi.signIn(login, password);
        this.saveAuthData(authData.userType, authData.userId);
    }

    private static saveAuthData(userType: string, userId: string) {
        localStorage.setItem("Auth", JSON.stringify({ userType, userId }));
    }

    public static async logOut(): Promise<void> {
        await AuthApi.logOut();
        localStorage.removeItem("Auth");
    }
}