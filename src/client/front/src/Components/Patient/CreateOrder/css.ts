import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
    header: {
        textAlign: 'center'
    },
    root: {
        height: '100%'
    },
    content: {
        width: 400
    },
    field: {
        width: '100%',
        marginTop: 20
    },
    buttons: {
        marginTop: 30
    }
});
