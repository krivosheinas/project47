import { Button, Grid, TextField, Typography } from "@material-ui/core"
import { useState } from "react";
import { useHistory } from "react-router-dom";
import { OrderApi } from "../../../Classes/Api/OrderApi";
import { useStyles } from "./css";

export const CreateOrder = () => {
    const styles = useStyles();
    const history = useHistory();
    const [symptoms, setSymptoms] = useState('');
    const [city, setCity] = useState('');
    
    const handleSymptomsChange = (event: any) => {
        setSymptoms(event.target.value);
    }

    const handleCityChange = (event: any) => {
        setCity(event.target.value);
    }

    const handleSubmit = async (event: any) => {
        event.preventDefault();
        await OrderApi.create(symptoms, city);
        history.push('/Patient/Orders');
    }

    const handleCancel = () => {
        history.push('/Patient/Orders');
    }

    return (
        <Grid className={styles.root} container alignItems='center' justifyContent='center'>
            <form onSubmit={handleSubmit}>
                <Grid container direction='column' alignItems='center' className={styles.content}>
                    <Typography className={styles.header} variant="h4">Создание заявки</Typography>
                    <TextField
                        id="symptoms"
                        label="Симптомы"
                        multiline
                        maxRows={4}
                        value={symptoms}
                        onChange={handleSymptomsChange}
                        className={styles.field}
                        required
                    />
                    <TextField
                        id="city"
                        label="Город"
                        value={city}
                        onChange={handleCityChange}
                        className={styles.field}
                        required
                    />
                    <Grid container justifyContent='center' spacing={2} className={styles.buttons}>
                        <Grid item>
                           <Button variant="contained" color="primary" type="submit">Создать</Button>
                        </Grid>
                        <Grid item>
                            <Button variant="contained" onClick={handleCancel}>Отмена</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </form>
        </Grid>
    )
}