import { Route, Switch, Redirect, useParams } from "react-router-dom";
import { Auth } from "../../Classes/Auth";
import { Exit } from "../Exit";
import { CreateOrder } from "./CreateOrder";
import { useStyles } from "./css";
import { PatientOrder } from "./PatientOrder";
import { PatientOrders } from "./PatientOrders";

export const Patient = () => {
    const styles = useStyles();

    if (!Auth.isPatient())
        return <Redirect to="/"></Redirect>

    return (
        <div className={styles.root}>
            <Exit whoami="patient"></Exit>
            <div className={styles.content}>
                <Switch>
                    <Route exact path="/Patient">
                        <Redirect to="/Patient/Orders"></Redirect>
                    </Route>
                    <Route exact path="/Patient/Orders">
                        <PatientOrders />
                    </Route>
                    <Route exact path="/Patient/Orders/Create">
                        <CreateOrder />
                    </Route>
                    <Route exact path="/Patient/Orders/:id">
                        <PatientOrder />
                    </Route>
                </Switch>
            </div>
        </div>
    );
}