import { Button, Card, CardActions, CardContent, Fab, Grid, Typography } from "@material-ui/core";
import moment from "moment";
import React from "react";
import { useEffect, useState } from "react";
import { OrderApi } from "../../../Classes/Api/OrderApi";
import { OrderDto } from "../../../Types/OrderDto";
import { useStyles } from "./css";
import AddIcon from '@material-ui/icons/Add';
import { Link, useHistory } from "react-router-dom";

export const PatientOrders = () => {
    const styles = useStyles();
    const history = useHistory();
    const [orders, setOrders] = useState<OrderDto[]>([]);

    useEffect(() => {
        (async () => {
            const fetchedOrders = await OrderApi.getPatientOrders();
            setOrders(fetchedOrders);
        })();
    },[]);

    const handleDelete = async (order: OrderDto) => {
        await OrderApi.delete(order.id);
        setOrders(orders.filter(x => x !== order));
    }

    const handleView = (order: OrderDto) => {
        history.push(`/Patient/Orders/${order.id}`);
    }

    return (
        <Grid container direction='column' alignItems='center'>
            <Typography className={styles.header} variant="h4">Мои заявки</Typography>
            {orders.map(x => {
                return (
                    <Card key={x.id} className={styles.card}>
                        <CardContent>
                            <Typography color="textSecondary">
                                {moment(x.dateCreated).format('DD.MM.YYYY')}
                            </Typography>
                            <Typography variant="h5">
                                {x.symptoms}
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Grid container justifyContent='space-between'>
                                <Button size="small" color="primary" onClick={() => handleView(x)}>Просмотр</Button>
                                <Button size="small" color="secondary" onClick={() => handleDelete(x)}>Удалить</Button>
                            </Grid>
                        </CardActions>
                    </Card>
                );
            })}
            <Link to='/Patient/Orders/Create'>
                <Fab className={styles.addButton} color="primary">
                    <AddIcon />
                </Fab>
            </Link>
        </Grid>
    );
}