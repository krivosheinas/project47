import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
    header: {
        marginBottom: 30,
        marginTop: 20
    },
    card: {
        width: 500,
        marginBottom: 20
    },
    addButton: {
        position: 'fixed',
        right: 20,
        bottom: 20
    }
});
