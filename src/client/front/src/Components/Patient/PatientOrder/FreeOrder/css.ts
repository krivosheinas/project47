import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
    header: {
        textAlign: 'center',
        marginTop: 20
    },
    root: {
        display: 'flex',
        minHeight: '100%'
    },
    content: {
        width: 400
    },
    field: {
        width: '100%',
        marginTop: 20
    },
    buttons: {
        marginTop: 30
    },
    part: {
        flexGrow: 1,
        marginLeft: 20,
        marginRight: 20
    },
    card: {
        width: 500,
        marginBottom: 20
    },
    respondsHeader: {
        marginBottom: 20
    }
});
