import { Button, Card, CardActions, CardContent, Divider, Grid, Snackbar, TextField, Typography } from "@material-ui/core"
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useStyles } from "./css";
import MuiAlert from '@material-ui/lab/Alert';
import moment from "moment";
import { OrderDto } from "../../../../Types/OrderDto";
import { RespondDto } from "../../../../Types/RespondDto";
import { RespondApi } from "../../../../Classes/Api/RespondApi";
import { OrderApi } from "../../../../Classes/Api/OrderApi";

interface FreeOrderProps {
    order: OrderDto;
    onOrderUpdated: (newOrder: OrderDto) => void;
}

export const FreeOrder = ({order, onOrderUpdated}: FreeOrderProps) => {
    const styles = useStyles();
    const history = useHistory();
    const [symptoms, setSymptoms] = useState('');
    const [city, setCity] = useState('');
    const [isUpdated, setIsUpdated] = useState(false);
    const [responds, setResponds] = useState<RespondDto[]>([]);
    
    useEffect(() => {
        setSymptoms(order.symptoms ?? '');
        setCity(order.city ?? '');
    }, [order]);

    useEffect(() => {
        (async () => {
            const fetchedResponds = await RespondApi.getRespondsByOrderId(order.id);
            setResponds(fetchedResponds);
        })();
    }, []);

    const handleSymptomsChange = (event: any) => {
        setSymptoms(event.target.value);
    }

    const handleCityChange = (event: any) => {
        setCity(event.target.value);
    }

    const handleSubmit = async (event: any) => {
        event.preventDefault();
        await OrderApi.edit(order.id, symptoms, city);
        onOrderUpdated({...order, symptoms, city});
        setIsUpdated(true);
    }

    const handleUpdatedClose = () => {
        setIsUpdated(false);
    }

    const handleBack = () => {
        history.push('/Patient/Orders');
    }

    const handleAccept = async (respond: RespondDto) => {
        await RespondApi.accept(respond.id);
        onOrderUpdated({...order, doctorId: respond.doctorId});
    }

    return (
        <div className={styles.root}>
            <Grid className={styles.part} container justifyContent='center'>
                <form onSubmit={handleSubmit}>
                    <Grid container direction='column' alignItems='center' className={styles.content}>
                        <Typography className={styles.header} variant="h4">Редактирование заявки</Typography>
                        <TextField
                            id="symptoms"
                            label="Симптомы"
                            multiline
                            maxRows={4}
                            value={symptoms}
                            onChange={handleSymptomsChange}
                            className={styles.field}
                            required
                        />
                        <TextField
                            id="city"
                            label="Город"
                            value={city}
                            onChange={handleCityChange}
                            className={styles.field}
                            required
                        />
                        <Grid container direction='column' alignItems='center' justifyContent='center' spacing={2} className={styles.buttons}>
                            <Grid item>
                                <Button variant="contained" color="primary" type="submit">Редактировать</Button>
                            </Grid>
                            <Grid item>
                                <Button variant="contained" onClick={handleBack}>Назад</Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </form>
            </Grid>
            <div>
                <Divider orientation="vertical" />
            </div>
            <Grid container direction='column' className={styles.part} alignItems='center'>
                <Typography className={[styles.header, styles.respondsHeader].join(' ')} variant="h4">Отклики врачей</Typography>
                {responds.length > 0 ?
                    responds.map(x => {
                        return (
                            <Card key={x.id} className={styles.card}>
                                <CardContent>
                                    <Typography color="textSecondary">
                                        {moment(x.dateCreated).format('DD.MM.YYYY')}
                                    </Typography>
                                    <Typography variant="h5">
                                        {x.comment}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Grid container justifyContent='flex-end'>
                                        <Button size="small" color="secondary" onClick={() => handleAccept(x)}>Принять</Button>
                                    </Grid>
                                </CardActions>
                            </Card>
                        );
                }) : <Typography variant="h5">Пусто</Typography>}
            </Grid>
            <Snackbar open={isUpdated} autoHideDuration={3000} onClose={handleUpdatedClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                    }}>
                    <MuiAlert elevation={6} variant="filled" onClose={handleUpdatedClose} severity="success">
                    Заявка обновлена
                </MuiAlert>
            </Snackbar>
        </div>
    )
}