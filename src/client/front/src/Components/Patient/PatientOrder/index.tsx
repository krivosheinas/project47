import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { OrderApi } from "../../../Classes/Api/OrderApi";
import { OrderDto } from "../../../Types/OrderDto";
import { FreeOrder } from "./FreeOrder";
import { Chat } from "../../Common/Chat";
import { Auth } from "../../../Classes/Auth";

export const PatientOrder = () => {
    const { id } = useParams<{id: string}>();
    const [order, setOrder] = useState<OrderDto | null>(null);
    const history = useHistory();
    
    useEffect(() => {
        (async () => {
            const fetchedOrder = await OrderApi.getById(id);
            setOrder(fetchedOrder);
        })();
    }, []);

    const handleOrderUpdated = (newOrder: OrderDto) => {
        setOrder(newOrder);
    }

    if (!order)
        return <></>;

    const isAccepted = !!order.doctorId;

    const getAuthorName = (authorId: string) => {
        if(authorId === Auth.getUserId())
            return 'Я';
        if(authorId === order.doctorId)
            return 'Доктор';
        return '';
    }

    if (isAccepted)
        return <Chat 
                    order={order} 
                    onGoBack={() => history.push('/Patient/Orders')}
                    header='Переписка с доктором'
                    orderAuthor='Я'
                    amIOrderAuthor
                    getAuthorName={getAuthorName}
                />
    
    return <FreeOrder order={order} onOrderUpdated={handleOrderUpdated} />
}