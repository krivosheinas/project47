import { Button, Grid } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { Auth } from "../../Classes/Auth";
import { useStyles } from "./css";

type TExitState = {whoami:string}

export const Exit: React.FC<TExitState> = ({whoami}) => { 
    const styles = useStyles();
    const history = useHistory();
    
    const handleExit = async () => {
        await Auth.logOut();
        history.push('/')
    }

    return (
        <>
            <Grid className={styles.container} container justifyContent="flex-end">
                 <h4 style={{color:"white"}}>{whoami}</h4>
                <Button color="secondary" onClick={handleExit}>Выход</Button>
            </Grid>
        </>
    );
}