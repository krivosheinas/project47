import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
    mainContainer: {
        width: '100%',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    login: {
        marginBottom: 20
    },
    registerButton: {
        width: '100%',
        marginTop: 20
    },
    header: {
        marginBottom: 10
    },
    cancelButton: {
        width: '100%',
        marginTop: 10
    },
    roleGroup: {
        marginBottom: 20
    },
    textField: {
        width: '100%'
    }
});
