import { Button, FormControlLabel, Grid, Radio, RadioGroup, TextField, Typography } from "@material-ui/core";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import { useStyles } from "./css";

const Register = () => {

    const styles = useStyles();
    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");
    const [role, setRole] = useState<'patient'|'doctor'|null>();
    const history = useHistory();

    const handleLoginChange = (e: any) => {
        setLogin(e.target.value);
    }

    const handlePasswordChange = (e: any) => {
        setPassword(e.target.value);
    }

    const handleSubmit = async (event: any) => {
        event.preventDefault();
        //TODO Реализовать регистрацию
    }

    const handleRoleChange = (event: any) => {
        setRole(event.target.value);
    }

    return (
        <>
            <div className={styles.mainContainer}>
                <form onSubmit={handleSubmit}>
                    <Grid
                        container
                        direction="column"
                        justifyContent="center"
                        alignItems="center"
                    >
                        <Typography className={styles.header} variant="h6">
                            Регистрация
                        </Typography>
                        <RadioGroup name="role" onChange={handleRoleChange} row className={styles.roleGroup}>
                            <FormControlLabel value="patient" control={<Radio />} label="Я пациент" />
                            <FormControlLabel value="doctor" control={<Radio />} label="Я доктор" />
                        </RadioGroup>
                        <TextField required className={[styles.login, styles.textField].join(' ')} label="Логин" variant="outlined" onChange={handleLoginChange} />
                        <TextField required className={styles.textField} label="Пароль" variant="outlined" type="password" onChange={handlePasswordChange} />
                        <Button className={styles.registerButton} variant="contained" color="primary" type="submit">Зарегистрироваться</Button>
                        <Button className={styles.cancelButton} onClick={() => history.push('/')}>Отмена</Button>
                    </Grid>
                </form>
            </div>
        </>
    );
}

export default Register;