import { Redirect } from "react-router-dom"
import { Auth } from "../../Classes/Auth"

export const Home = () => {
    if (Auth.isPatient())
        return <Redirect to='/Patient'></Redirect>;
    else if (Auth.isDoctor())
        return <Redirect to='/Doctor'></Redirect>;
    else
        return <Redirect to='/Login'></Redirect>;
}