import { Route, Switch, Redirect } from "react-router-dom";
import { Auth } from "../../Classes/Auth";
import { Exit } from "../Exit";
import { useStyles } from "./css";
import DoctorOrder from "./DoctorOrder";
import { DoctorOrders } from "./DoctorOrders";

export const Doctor = () => {
    const styles = useStyles();

    if (!Auth.isDoctor())
        return <Redirect to="/"></Redirect>

    return (
        <div className={styles.root}>
            <Exit whoami="doctor"></Exit>
            <div className={styles.content}>
                <Switch>
                    <Route exact path="/Doctor">
                        <Redirect to="/Doctor/Orders"></Redirect>
                    </Route>
                    <Route exact path="/Doctor/Orders">
                        <DoctorOrders />
                    </Route>
                    <Route exact path="/Doctor/Orders/:id">
                        <DoctorOrder />
                    </Route>
                </Switch>
            </div>
        </div>
    );
}