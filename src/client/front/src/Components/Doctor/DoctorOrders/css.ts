import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
    header: {
        textAlign: 'center',
        marginBottom: 20
    },
    root: {
        display: 'flex',
        minHeight: '100%'
    },
    part: {
        flex: '1 0',
        marginLeft: 20,
        marginRight: 20,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 20
    },
    card: {
        width: 500,
        marginBottom: 20
    },
});
