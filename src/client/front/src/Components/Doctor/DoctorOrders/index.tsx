import { Button, Card, CardActions, CardContent, Dialog, DialogActions, DialogContent, DialogTitle, Divider, Grid, TextField, Typography } from "@material-ui/core";
import moment from "moment";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { OrderApi } from "../../../Classes/Api/OrderApi";
import { RespondApi } from "../../../Classes/Api/RespondApi";
import { OrderDto } from "../../../Types/OrderDto";
import { useStyles } from "./css";

export const DoctorOrders = () => {
    const styles = useStyles();
    const history = useHistory();
    const [freeOrders, setFreeOrders] = useState<OrderDto[]>([]);
    const [doctorOrders, setDoctorOrders] = useState<OrderDto[]>([]);
    const [openRespond, setOpenRespond] = useState<boolean>(false);
    const [orderToRespond, setOrderToRespond] = useState<OrderDto | null>(null);
    const [respond, setRespond] = useState('');

    useEffect(() => {
        (async () => {
            const fetched = await OrderApi.getFreeOrders();
            setFreeOrders(fetched);
        })();
        (async () => {
            const fetched = await OrderApi.getDoctorOrders();
            setDoctorOrders(fetched);
        })();
    }, []);

    const handleRespond = (order: OrderDto) => {
        setOrderToRespond(order);
        setRespond('');
        setOpenRespond(true);
    }

    const handleRespondChange = (event: any) => {
        setRespond(event.target.value);
    }

    const handleSendRespond = async () => {
        await RespondApi.add(orderToRespond!.id, respond);
        setOpenRespond(false);
    }

    const handleChatting = (order: OrderDto) => {
        history.push(`/Doctor/Orders/${order.id}`);
    }

    const handleCloseOrder = async (order: OrderDto) => {
        await OrderApi.close(order.id);
        setDoctorOrders(doctorOrders.filter(o => o !== order));
    }

    return (
        <>
            <div className={styles.root}>
                <div className={styles.part}>
                    <Typography className={styles.header} variant="h4">Свободные заявки</Typography>
                    {freeOrders.map(x => {
                        return (
                            <Card key={x.id} className={styles.card}>
                                <CardContent>
                                    <Grid container justifyContent="space-between">
                                        <Typography color="textSecondary">
                                            {moment(x.dateCreated).format('DD.MM.YYYY')}
                                        </Typography>
                                        <Typography color="textSecondary">
                                            Город: {x.city}
                                        </Typography>
                                    </Grid>
                                    <Typography variant="h5">
                                        {x.symptoms}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Grid container justifyContent='space-between'>
                                        <Button size="small" color="primary" onClick={() => handleRespond(x)}>Откликнуться</Button>
                                    </Grid>
                                </CardActions>
                            </Card>
                        );
                    })}
                </div>
                <div>
                    <Divider orientation="vertical" />
                </div>
                <div className={styles.part}>
                    <Typography className={styles.header} variant="h4">Мои заявки</Typography>
                    {doctorOrders.map(x => {
                        return (
                            <Card key={x.id} className={styles.card}>
                                <CardContent>
                                    <Grid container justifyContent="space-between">
                                        <Typography color="textSecondary">
                                            {moment(x.dateCreated).format('DD.MM.YYYY')}
                                        </Typography>
                                        <Typography color="textSecondary">
                                            Город: {x.city}
                                        </Typography>
                                    </Grid>
                                    <Typography variant="h5">
                                        {x.symptoms}
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                    <Grid container justifyContent='space-between'>
                                        <Button size="small" color="primary" onClick={() => handleChatting(x)}>Переписка</Button>
                                        <Button size="small" color="secondary" onClick={() => handleCloseOrder(x)}>Закрыть заявку</Button>
                                    </Grid>
                                </CardActions>
                            </Card>
                        );
                    })}
                </div>
            </div>
            <Dialog open={openRespond} onClose={() => setOpenRespond(false)}>
                <DialogTitle>Введите сообщение пациенту</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        placeholder="Сообщение"
                        fullWidth
                        multiline
                        maxRows={10}
                        onChange={handleRespondChange}
                        value={respond}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setOpenRespond(false)} color="primary">
                        Отмена
                    </Button>
                    <Button onClick={handleSendRespond} color="primary">
                        Отправить
                    </Button>
                </DialogActions>
            </Dialog>
      </>
    );
}