import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { OrderApi } from "../../../Classes/Api/OrderApi";
import { Auth } from "../../../Classes/Auth";
import { OrderDto } from "../../../Types/OrderDto";
import { Chat } from "../../Common/Chat";

const DoctorOrder = () => {
    const { id } = useParams<{id: string}>();
    const [order, setOrder] = useState<OrderDto | null>(null);
    const history = useHistory();
    
    useEffect(() => {
        (async () => {
            const fetchedOrder = await OrderApi.getById(id);
            setOrder(fetchedOrder);
        })();
    }, []);

    if (!order)
        return <></>;

    const getAuthorName = (authorId: string) => {
        if(authorId === Auth.getUserId())
            return 'Я';
        if(authorId === order.patientId)
            return 'Пациент';
        return '';
    }

    return <Chat 
                order={order} 
                onGoBack={() => history.push('/Doctor/Orders')}
                header='Переписка с пациентом'
                orderAuthor='Пациент'
                amIOrderAuthor={false}
                getAuthorName={getAuthorName}
            />
}

export default DoctorOrder;