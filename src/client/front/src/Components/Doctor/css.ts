import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        height: '100%',
    },
    content: {
        overflow: 'auto',
        width: '100%',
        height: '100%',
    }
});
