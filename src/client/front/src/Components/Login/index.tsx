import { Button, Grid, Snackbar, TextField, Typography } from "@material-ui/core";
import { useState } from "react";
import { Redirect, useHistory } from "react-router-dom";
import { Auth } from "../../Classes/Auth";
import { useStyles } from "./css";
import MuiAlert from '@material-ui/lab/Alert';

const Login = () => {

    const styles = useStyles();
    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");
    const [isInvalid, setIsInvalid] = useState(false);
    const history = useHistory();

    const handleLoginChange = (e: any) => {
        setLogin(e.target.value);
    }

    const handlePasswordChange = (e: any) => {
        setPassword(e.target.value);
    }

    const handleSubmit = async (event: any) => {
        event.preventDefault();
        try {
            await Auth.signIn(login, password);
            history.push('/');
        }
        catch {
            setIsInvalid(true);
        }
    }

    const handleInvalidClose = () => {
        setIsInvalid(false);
    }

    return (
        <>
            <div className={styles.mainContainer}>
                <form onSubmit={handleSubmit}>
                    <Grid
                        container
                        direction="column"
                        justifyContent="center"
                        alignItems="center"
                    >
                        <Typography className={styles.header} variant="h6">
                            Вход в систему
                        </Typography>
                        <TextField required className={styles.login} label="Логин" variant="outlined" onChange={handleLoginChange} />
                        <TextField required label="Пароль" variant="outlined" type="password" onChange={handlePasswordChange} />
                        <Button className={styles.loginButton} variant="contained" color="primary" type="submit">Вход</Button>
                        <Button className={styles.registerButton} onClick={() => history.push('/Register')}>Регистрация</Button>
                    </Grid>
                </form>
            </div>
            <Snackbar open={isInvalid} autoHideDuration={3000} onClose={handleInvalidClose}>
                <MuiAlert elevation={6} variant="filled" onClose={handleInvalidClose} severity="error">
                    Неверный логин или пароль
                </MuiAlert>
            </Snackbar>
        </>
    );
}

export default Login;