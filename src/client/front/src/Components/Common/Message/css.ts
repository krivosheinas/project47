import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
    message: {
        width: 400,
        marginBottom: 20
    },
    left: {
        backgroundColor: '#c5fdc4'
    },
    right: {
        backgroundColor: '#adddff'
    },
    author: {
        fontWeight: 'bold'
    },
    text: {
        marginTop: 10,
        lineHeight: 1.2
    },
    input: {
        width: '100%',
        marginTop: 4,
        marginBottom: -6,
        '& .MuiInput-input': {
            lineHeight: 1.2
        }
    }
});
