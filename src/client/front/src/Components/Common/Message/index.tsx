import { Button, Card, CardActions, CardContent, Grid, TextField, Typography } from "@material-ui/core";
import moment from "moment";
import { useEffect, useState } from "react";
import { useStyles } from "./css";

interface MessageProps {
    editable: boolean;
    author: string;
    align: 'left' | 'right';
    date: Date;
    text: string;
    onDelete?: () => void;
    onTextChanged?: (newText: string) => void;
}

export const Message = ({editable, author, align, date, text, onDelete, onTextChanged}: MessageProps) => {
    const styles = useStyles();

    const [isEditMode, setIsEditMode] = useState(false);
    const [newText, setNewText] = useState('');

    useEffect(() => {
        setNewText(text);
    }, [isEditMode]);

    const handleInputChange = (event: any) => {
        setNewText(event.target.value);
    }

    const handleEdit = () => {
        setIsEditMode(true);
    }

    const handleSave = () => {
        setIsEditMode(false);
        onTextChanged && onTextChanged(newText);
    }

    return (
        <Grid container justifyContent={align === 'left' ? 'flex-start' : 'flex-end'}>
            <Card className={[styles.message, align === 'left' ? styles.left : styles.right].join(' ')}>
                <CardContent>
                    <Grid container justifyContent="space-between">
                        <Typography variant='body2' className={styles.author}>
                            {author}
                        </Typography>
                        <Typography variant='body2' color="textSecondary">
                            {moment(date).format('DD.MM.YYYY HH:mm:ss')}
                        </Typography>
                    </Grid>
                    {isEditMode ?
                        <TextField
                            id="input"
                            multiline
                            maxRows={10}
                            value={newText}
                            onChange={handleInputChange}
                            className={styles.input}
                            required
                            placeholder="Сообщение"
                            autoFocus
                        />
                        :
                        <Typography className={styles.text} variant="body1">
                            {text}
                        </Typography>
                    }
                </CardContent>
                {editable && 
                    <CardActions>
                        <Grid container justifyContent="flex-end">
                            {isEditMode ?
                                <>
                                    <Button size="small" onClick={handleSave}>Сохранить</Button>
                                    <Button size="small" onClick={() => setIsEditMode(false)}>Отмена</Button>
                                </>
                                :
                                <>
                                    <Button size="small" onClick={handleEdit}>Редактировать</Button>
                                    <Button size="small" onClick={() => onDelete && onDelete()}>Удалить</Button>
                                </>}                           
                        </Grid>
                    </CardActions>
                }
            </Card>
        </Grid>
    )
}