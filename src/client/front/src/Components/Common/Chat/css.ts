import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        height: '100%',
        position: 'relative',
        paddingTop: 20,
        boxSizing: "border-box"
    },
    messagesScrollArea: {
        width: '100%',
        height: '100%',
        overflowY: 'auto',
        overflowX: 'hidden'
    },
    header: {
        marginBottom: 10,
        textAlign: 'center'
    },
    messages: {
        width: 700,
        paddingTop: 20
    },
    divider: {
        width: '100%'
    },
    inputArea: {
        padding: 15,
        display: 'flex',
        width: 'calc(100% - 30px)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        width: 700,
    },
    backButton: {
        position: 'absolute',
        left: 20
    }
});
