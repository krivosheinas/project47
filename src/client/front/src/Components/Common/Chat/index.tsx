import { Button, Divider, Grid, IconButton, TextField, Typography } from "@material-ui/core";
import { ArrowBack, Send } from "@material-ui/icons";
import { useEffect, useRef, useState } from "react";
import { MessagesApi } from "../../../Classes/Api/MessageApi";
import { Auth } from "../../../Classes/Auth";
import { MessageDto } from "../../../Types/MessageDto";
import { OrderDto } from "../../../Types/OrderDto";
import { Message } from "../Message";
import { useStyles } from "./css";

interface ChatProps {
    order: OrderDto;
    onGoBack: () => void;
    header: string;
    orderAuthor: string;
    amIOrderAuthor: boolean;
    getAuthorName: (authorId: string) => string;
}

export const Chat = ({ order, onGoBack, header, orderAuthor, amIOrderAuthor, getAuthorName }: ChatProps) => {
    const styles = useStyles();
    const [messages, setMessages] = useState<MessageDto[]>([]);
    const [input, setInput] = useState<string>('');
    const messagesEndRef = useRef<HTMLDivElement | null>(null);
    const [isFirstScroll, setIsFirstScroll] = useState<boolean>(true);

    useEffect(() => {
        (async () => {
            const fetchedMessages = await MessagesApi.get(order.id);
            setMessages(fetchedMessages);
        })();
    }, []);

    useEffect(() => {
        if (messages.length && isFirstScroll) {
            messagesEndRef.current?.scrollIntoView({ behavior: "auto" });
            setIsFirstScroll(false);
        }
    }, [messages]);

    const handleInputChange = (event: any) => {
        setInput(event.target.value);
    }

    const handleSend = async () => {
        if (input.length) {
            const newMessage = await MessagesApi.send(order.id, input);
            setMessages([...messages, newMessage]);
            setInput('');
            messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
        }
    }

    const handleDeleteMessage = async (message: MessageDto) => {
        await MessagesApi.delete(message.id);
        setMessages(messages.filter(x => x !== message));
    }

    const handleMessageChanged = async (newText: string, message: MessageDto) => {
        await MessagesApi.update(message.id, newText);
        setMessages(messages.map(m => m === message ? {...message, text: newText} : m ));
    }

    return (
        <div className={styles.root}>
            <Typography className={styles.header} variant="h4">{header}</Typography>
            <Button
                variant="contained"
                color="secondary"
                className={styles.backButton}
                startIcon={<ArrowBack />}
                onClick={onGoBack}
            >
                Назад
            </Button>
            <Divider orientation="horizontal" className={styles.divider} />
            <Grid container direction='column' className={styles.messagesScrollArea} alignItems='center'>
                <Grid container direction='column' className={styles.messages}>
                    <Message align={amIOrderAuthor ? 'right' : 'left'} author={orderAuthor} date={order.dateCreated} text={order.symptoms} editable={false} />
                    {messages.map(message => {
                        if(message.authorId === Auth.getUserId())
                            return <Message key={message.id} align='right' author={getAuthorName(message.authorId)} date={message.dateTime} text={message.text} editable={true}
                                        onDelete={() => handleDeleteMessage(message)}
                                        onTextChanged={(newText: string) => handleMessageChanged(newText, message)} />
                        return <Message key={message.id} align='left' author={getAuthorName(message.authorId)} date={message.dateTime} text={message.text} editable={false} />
                    })}
                    <div ref={messagesEndRef} />
                </Grid>
            </Grid>
            <Divider orientation="horizontal" className={styles.divider} />
            <div className={styles.inputArea}>
                <TextField
                    id="input"
                    multiline
                    maxRows={10}
                    value={input}
                    onChange={handleInputChange}
                    className={styles.input}
                    required
                    placeholder="Сообщение"
                />
                <IconButton color="primary" component="span" onClick={handleSend}>
                    <Send />
                </IconButton>
            </div>
        </div>
    );
}