import { Button, Typography } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { useStyles } from "./css";

export const NotFound = () => {
    const styles = useStyles();
    const history = useHistory();

    return (
        <div className={styles.mainContainer}>
            <Typography variant="h3">
                Такой страницы не существует
            </Typography>
            <Button className={styles.button} variant="contained" color="primary" onClick={() => history.push('/')}>К началу</Button>
        </div>
    );
}