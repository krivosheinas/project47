import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
    mainContainer: {
        width: '100%',
        height: '100vh',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        marginTop: 50
    }
});
